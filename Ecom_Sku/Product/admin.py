from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Products_options)
admin.site.register(Products_variation_combinations)
admin.site.register(Sku)
admin.site.register(Products_variations)
admin.site.register(Products_variations_options)
admin.site.register(Zone)
admin.site.register(Pincode)
admin.site.register(Wishlist)
class productAdmin(admin.ModelAdmin):
    list_display = ['product_name','shop']
  
    def active(self, obj):
        return obj.is_active == 1
  
    active.boolean = True
admin.site.register(Product,productAdmin) 