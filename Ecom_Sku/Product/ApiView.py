from itertools import combinations
from random import choice
from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
from rest_framework.response import Response 
from rest_framework.decorators import api_view,permission_classes

from Category.ApiView import subcategory
from .serializers import *
from .models import *
from collections import OrderedDict 
from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import AllowAny
@api_view(['GET'])
def sizechart(request,sizechart):
    if sizechart=='dress_sizechart':
        d=Dresschart.objects.all()
        serializer=dresserializer(d,many=True)
        return Response(serializer.data)
    elif sizechart=='footware_sizechart':
        f=Footwearchart.objects.all()
        serializer=footserializer(f,many=True)
        return Response(serializer.data)
    else:
        return Response('no size chart for this input')
@api_view(['GET'])
def listProducts(request,shid):
    p=Product.objects.filter(shop=shid).order_by('-id')
    
    serializer=productserializer(p,many=True)
    return Response (serializer.data)

@api_view(['GET'])
def RetriveSku(request,skuid):
    p=Sku.objects.filter(id=skuid).order_by('-id')
    
    serializer=skuserializercc(p,many=True)
    return Response (serializer.data)

@api_view(['GET'])
def listProductscust(request,shid):
    pro=[]
    p=Product.objects.filter(shop=shid)
    ps=0
    s=0
    o=0
    for i in p:
        sku=Sku.objects.filter(product_id=i.id)
        for j in sku:
            ps=j.price
            s=j.sales_rate
            o=j.offer
            skid=j.id
            print(p)
        data={
            "id":i.id,
            "skid":skid,
            "productname":i.product_name,
            "maincategory":i.maincategory.category_name,
            "subcategory":i.subcategory_id,
            "product_image":i.product_image1.url,
            "brand":i.brand_name,
            "price":ps,
            "salesrate":s,
            "offer":o,
        }
        pro.append(data)
    # serializer=productserializer(p,many=True)
    return Response (pro)
@api_view(['GET'])
def RetrieveProducts(request,productid):
    p=Product.objects.filter(id=productid)
    
    serializer=productserializer(p,many=True)
    return Response (serializer.data)


@api_view(['GET'])
def RetrieveProductsCust(request,productid):
    p=Product.objects.get(id=productid)
    if p.product_image1:
        im1=p.product_image1.url
    else:
        im1=None
    if p.product_image2:
        im2=p.product_image2.url
    else:
        im2=None
    if p.product_image3:
        im3=p.product_image3.url
    else:
        im3=None
    if p.banner1:
        b1=p.banner1.url
    else:
        b1=None
    if p.banner2:
        b2=p.banner2.url
    else:
        b2=None
    if p.subcategory:
        sub=p.subcategory.category_name
    else:
        sub=None
    skus=Sku.objects.filter(product_id=productid).values()
    skus1=Sku.objects.filter(product_id=productid)
    l=[]
    oh=None
    sts=None
    sr=[]
    # for i in skus1:
        
    #     primary=Products_variation_combinations.objects.filter(id=i.products_variation_combinations_id_id)
    #     secondary=Products_variation_combinations.objects.filter(id=i.products_variation_combinations_id_secondary_id)
    #     for j in primary:
             
    #         options=Products_variations_options.objects.filter(id=j.product_variations_options_id_id).values()
    #         optionsqw=Products_variations_options.objects.filter(id=j.product_variations_options_id_id)
    #         for x in optionsqw:
    #             type=Products_variations.objects.filter(id=x.product_variation_id_id).values()
    #             ty=Products_variations.objects.filter(id=x.product_variation_id_id)
    #             st=x.variationOptionName
    #             for n in ty:
    #                 print("---",n.variation_name)
    #     for k in secondary:
             
    #         options1=Products_variations_options.objects.filter(id=k.product_variations_options_id_id).values()
    #         optionhqw=Products_variations_options.objects.filter(id=k.product_variations_options_id_id)
    #         for y in optionhqw:
    #             type1=Products_variations.objects.filter(id=y.product_variation_id_id).values()
    #             tys=Products_variations.objects.filter(id=y.product_variation_id_id)
    #             sts=y.variationOptionName
                
    #             for m in tys:
                
    #                 oh=m.variation_name
    for sk in skus1:
        print("--------",sk.products_variation_combinations_id_id)
        primary=Products_variation_combinations.objects.filter(id=sk.products_variation_combinations_id_id)
        secondary=Products_variation_combinations.objects.filter(id=sk.products_variation_combinations_id_secondary_id)
        for pr in primary:
             
            options=Products_variations_options.objects.filter(id=pr.product_variations_options_id_id).values()
            optionsqw=Products_variations_options.objects.filter(id=pr.product_variations_options_id_id)
            for x in optionsqw:
                type=Products_variations.objects.filter(id=x.product_variation_id_id).values()
                ty=Products_variations.objects.filter(id=x.product_variation_id_id)
                st=x.variationOptionName
                for n in ty:
                    print(n.variation_name)
        for k in secondary:
             
            options1=Products_variations_options.objects.filter(id=k.product_variations_options_id_id).values()
            optionhqw=Products_variations_options.objects.filter(id=k.product_variations_options_id_id)
            for y in optionhqw:
                type1=Products_variations.objects.filter(id=y.product_variation_id_id).values()
                tys=Products_variations.objects.filter(id=y.product_variation_id_id)
                sts=y.variationOptionName
                
                for m in tys:
                
                    oh=m.variation_name
        data1={
            "id":sk.id,
            "sku_name":sk.sku_name,
            "price":sk.price,
            "salesrate":sk.sales_rate,
            "productname":sk.product_id.product_name,
            "offer":sk.offer,
            "stock":sk.stock,
            "primaryname":n.variation_name,
            "primaryvalues":st,
            "secondaryname":oh,
            
            "secondaryvalues":sts,
            
        }
        l.append(data1)
        

          
        
    
    pincodes=Product.objects.filter(id=productid)
    print(pincodes.values())
    data={
        "id":p.id,
        "productname":p.product_name,
        
        "product_image1":im1,
        "product_image2":im2,
        "product_image3":im3,
        "description":p.product_description,
        "Higlights":p.highlights,
        "return_policy":p.return_policy,
        "No _of_days_refund":p.number_of_days_for_refund,
        "Banner1":b1,
        "Banner2":b2,
        "Net_weight":p.Net_weight,
        "Brandname":p.brand_name,
        "refund":p.refund,
        "delivery_days":p.delivery_days,
        "maincategory ":p.maincategory.category_name, 
        "subcategory ":sub, 
        "unit":p.unit,
        "cupon_code":p.coupon_code,
        "featured":p.featured,
        "price":l[0]['salesrate'],
        "primaryname":l[0]['primaryname'],
        "primaryvalue":l[0]['primaryvalues'],
        "secondaryname":l[0]['secondaryname'],
        "secondaryvalue":l[0]['secondaryvalues'],
        "sku":l,
        "combinations":l,
        


    }
    
    # serializer=productserializer(p,many=True)
    return Response (data)

@api_view(['POST'])
def RetrieveProductsbyname(request):
    if request.method=='POST':
        name=request.data['name']
        shid=request.data['shid']
        p=Product.objects.filter(product_name__contains=name,shop=shid)
        
        serializer=productserializer(p,many=True)
        return Response (serializer.data)

@api_view(['POST'])
def RetrieveProductsbynameAndCat(request):
    if request.method=='POST':
        name=request.data['name']
        shid=request.data['shid']
        catid=request.data['catid']
        subcatid=request.data['subcatid']
        pro=[]
        if subcatid:
            p=Product.objects.filter(Q(product_name__contains=name)&Q(shop=shid)&Q(maincategory__id=catid)&Q(subcategory__id=subcatid))
        else:
            p=Product.objects.filter(Q(product_name__contains=name)&Q(shop=shid)&Q(maincategory__id=catid))
        print(p)
        ps=0
        s=0
        o=0
        for i in p:
            sku=Sku.objects.filter(product_id=i.id)
            for j in sku:
                ps=j.price
                s=j.sales_rate
                o=j.offer
                skid=j.id
                print(p)
            data={
                "id":i.id,
                "skid":skid,
                "productname":i.product_name,
                "maincategory":i.maincategory.category_name,
                "subcategory":i.subcategory_id,
                "product_image":i.product_image1.url,
                "brand":i.brand_name,
                "price":ps,
                "salesrate":s,
                "offer":o,
                "Featured":i.featured
            }
            pro.append(data)
       
        return Response (pro)

        

@api_view(['GET'])
def RetrieveFeaturedProducts(request,shid):
    
    pro=[]
    p=Product.objects.filter(shop=shid,featured=True)
    ps=0
    s=0
    o=0
    for i in p:
        sku=Sku.objects.filter(product_id=i.id)
        for j in sku:
            ps=j.price
            s=j.sales_rate
            o=j.offer
            skid=j.id
            print(p)
        data={
            "id":i.id,
            "skid":skid,
            "productname":i.product_name,
            "maincategory":i.maincategory.category_name,
            "subcategory":i.subcategory_id,
            "product_image":i.product_image1.url,
            "brand":i.brand_name,
            "price":ps,
            "salesrate":s,
            "offer":o,
            "Featured":i.featured
        }
        pro.append(data)
    # print(p)
    # serializer=productserializer(p,many=True)
    return Response (pro)
@api_view(['GET'])
def products_by_category(request,catid,shid):
    p=Product.objects.filter(maincategory=catid,shop=shid).order_by('-id')
    serializer=productserializer(p,many=True)
    return Response (serializer.data)

@api_view(['GET'])
def products_by_subcategory(request,scatid,shid):
    p=Product.objects.filter(subcategory=scatid,shop=shid).order_by('-id')
    serializer=productserializer(p,many=True)
    return Response (serializer.data)

@api_view(['GET'])
def productdessort(request,shid):
    pro=[]
    p=Product.objects.filter(shop=shid).order_by('-id')
    ps=0
    s=0
    o=0
    for i in p:
        sku=Sku.objects.filter(product_id=i.id)
        for j in sku:
            ps=j.price
            s=j.sales_rate
            o=j.offer
            skid=j.id
            print(p)
        data={
            "id":i.id,
            "skid":skid,
            "productname":i.product_name,
            "maincategory":i.maincategory.category_name,
            "subcategory":i.subcategory_id,
            "product_image":i.product_image1.url,
            "brand":i.brand_name,
            "price":ps,
            "salesrate":s,
            "offer":o,
            "Featured":i.featured
        }
        pro.append(data)
    # print(p)
    # serializer=productserializer(p,many=True)
    return Response (pro)

@api_view(['GET'])
def productascsort(request,shid):
    p=Product.objects.filter(shop=shid).order_by('id')
    serializer=productserializer(p,many=True)
    return Response (serializer.data)

@api_view(['POST'])
def productbypricerange(request):
    if request.method=='POST':
        p1=request.data['from']
        p2=request.data['to']
        shid=request.data['shid']
        p=Product.objects.filter(sku__sales_rate__range=(p1,p2),shop=shid).order_by('id')
        serializer=productserializer(p,many=True)
        return Response (serializer.data)

@api_view(['GET'])
def Size(request):
    p=Products_variations_options.objects.filter(product_variation_id__variation_name="Size").order_by('id')
    serializer=productvariationsoptionserializer(p,many=True)
    return Response (serializer.data)

@api_view(['GET'])
def Color(request):
    p=Products_variations_options.objects.filter(product_variation_id__variation_name="Color").order_by('id')
    serializer=productvariationsoptionserializer(p,many=True)
    return Response (serializer.data)



@api_view(['GET'])
def listskuofproduct(request,pid):
    p=Sku.objects.filter(product_id=pid)
    r=[]
    oh=0
    sts=0
    
    for i in p:
        primary=Products_variation_combinations.objects.filter(id=i.products_variation_combinations_id_id)
        secondary=Products_variation_combinations.objects.filter(id=i.products_variation_combinations_id_secondary_id)
        for j in primary:
             
            options=Products_variations_options.objects.filter(id=j.product_variations_options_id_id).values()
            optionsqw=Products_variations_options.objects.filter(id=j.product_variations_options_id_id)
            for x in optionsqw:
                type=Products_variations.objects.filter(id=x.product_variation_id_id).values()
                ty=Products_variations.objects.filter(id=x.product_variation_id_id)
                st=x.variationOptionName
                for n in ty:
                    print(n.variation_name)
        for k in secondary:
                
            options1=Products_variations_options.objects.filter(id=k.product_variations_options_id_id).values()
            optionhqw=Products_variations_options.objects.filter(id=k.product_variations_options_id_id)
            for y in optionhqw:
                type1=Products_variations.objects.filter(id=y.product_variation_id_id).values()
                tys=Products_variations.objects.filter(id=y.product_variation_id_id)
                sts=y.variationOptionName
                
                for m in tys:
                
                    oh=m.variation_name
            
        data={
            "id":i.id,
            "sku_name":i.sku_name,
            "price":i.price,
            "salesrate":i.sales_rate,
            "productname":i.product_id.product_name,
            "offer":i.offer,
            "stock":i.stock,
            "primaryname":n.variation_name,
            "primaryvalues":st,
            "secondaryname":oh,
            
            "secondaryvalues":sts,
            
        }
        r.append(data)
    # serializer=Skuserializer(p,many=True)
    return Response (r)



@api_view(['POST'])
def onchange(request):
    if request.method=='POST':
        
        shid=request.data['shid']
        catid=request.data['catid']
        subcatid=request.data['subcatid']
        pid=request.data['pid']
        sid=request.data['sid']
        pro=[]
        sro=[]
        if catid and subcatid and pid and sid:
            p=Sku.objects.filter(Q(product_id__shop=shid)&Q(product_id__maincategory__id=catid)&Q(product_id__subcategory__id=subcatid)&Q(products_variation_combinations_id=pid)&Q(products_variation_combinations_id_secondary_id=sid))
            print("LOOP1")
        elif catid and pid and sid:
            p=Sku.objects.filter(Q(product_id__shop=shid)&Q(product_id__maincategory__id=catid)&Q(products_variation_combinations_id=pid)&Q(products_variation_combinations_id_secondary_id=sid))
            print("LOOP2") 
        elif catid and pid:
            p=Sku.objects.filter(Q(product_id__shop=shid)&Q(product_id__maincategory__id=catid)&Q(products_variation_combinations_id=pid))
            print("LOOP3")
        elif catid and subcatid:
            for r in catid:
                for s in subcatid:
                    p=Sku.objects.filter(Q(product_id__shop=shid)&Q(product_id__maincategory__id=r)&Q(product_id__subcategory__id=s))
                    print("LOOP4")
                    print(r)
                    print(s)
                    sro.append(p)
            print("oooooo",sro)
        elif catid:
            
            p=Sku.objects.filter(Q(product_id__shop=shid)&Q(product_id__maincategory__id=catid))
            print("LOOP5")
                
        else :
            p=Sku.objects.filter(product_id__shop=shid)
            print("LOOP6")
        
        
        
        for p in sro :
            for i in p:
                print("xxxxx",i)
            
                
                primary=Products_variation_combinations.objects.filter(id=i.products_variation_combinations_id_id)
                secondary=Products_variation_combinations.objects.filter(id=i.products_variation_combinations_id_secondary_id)
                print(secondary)
                for j in primary:
                    
                    options=Products_variations_options.objects.filter(id=j.product_variations_options_id_id).values()
                    optionsqw=Products_variations_options.objects.filter(id=j.product_variations_options_id_id)
                    for x in optionsqw:
                        type=Products_variations.objects.filter(id=x.product_variation_id_id).values()
                        ty=Products_variations.objects.filter(id=x.product_variation_id_id)
                        st=x.variationOptionName
                        for n in ty:
                            print(n.variation_name)
                for k in secondary:
                    
                    options1=Products_variations_options.objects.filter(id=k.product_variations_options_id_id).values()
                    optionhqw=Products_variations_options.objects.filter(id=k.product_variations_options_id_id)
                    for y in optionhqw:
                        type1=Products_variations.objects.filter(id=y.product_variation_id_id).values()
                        tys=Products_variations.objects.filter(id=y.product_variation_id_id)
                        sts=y.variationOptionName
                        
                        for m in tys:
                        
                            oh=m.variation_name
            
                data={
                    "id":i.id,
                    "productname":i.product_id.product_name,
                    "maincategory":i.product_id.maincategory.category_name,
                    "subcategory":i.product_id.subcategory_id,
                    "product_image":i.product_id.product_image1.url,   
                    "brand":i.product_id.brand_name,
                    "price":i.price,
                    "salesrate":i.sales_rate,
                    "offer":i.offer,
                    "primaryname":n.variation_name,
                    "primaryvalues":st,
                    "secondaryname":m.variation_name,
                    "secondaryvalues":sts,
                    
                }
                pro.append(data)
        return Response (pro)

@api_view(['POST'])
def onchangescatandsubcat(request):
    if request.method=='POST':
        catid=request.data['catid']
        print("catid",catid)
        # print("subcatid",subcatid)
        subcat=[]
        if catid :
            for i in catid:
                print(";;;;",i)
                f=Category.objects.filter(id=i)
                for k in f:
                    mc=Category.objects.filter(parent=k.id).values()  
                    mc1=Category.objects.filter(parent=k.id)
                    for j in mc1:
                        data={
                            "id":j.id,
                            "parent_id":k.id,
                            
                            "category_name":j.category_name,
                            "category_slug":j.category_slug,
                            "category_image":j.category_image.url,
                            "icon_image":j.icon_image.url,
                            "category_description":j.category_description
                            
                        }
                        print(data)
                        subcat.append(data)  
            return Response(subcat)
        elif catid=='':
            t=Category.objects.filter(parent=None)
            for i in t:
                print(";;;;",i)
                mc=Category.objects.filter(parent=i.id)
                for j in mc:
                    data={
                            "id":j.id,
                            "parent_id":i.id,
                            
                            "category_name":j.category_name,
                            "category_slug":j.category_slug,
                            "category_image":j.category_image.url,
                            "icon_image":j.icon_image.url,
                            "category_description":j.category_description
                            
                        }
                    print(data)
                    subcat.append(data)  
            return Response(subcat)
@api_view(['GET'])
def getfullsubcat(request):
    
    subcat=[]
    t=Category.objects.filter(parent=None)
    for i in t:
        print(";;;;",i)
        mc=Category.objects.filter(parent=i.id)
        for j in mc:
            data={
                "id":j.id,
                "parent_id":i.id,
                
                "category_name":j.category_name,
                "category_slug":j.category_slug,
                "category_image":j.category_image.url,
                "icon_image":j.icon_image.url,
                "category_description":j.category_description
            }
            print(data)
            subcat.append(data)  
    return Response(subcat)

@api_view(['POST'])
def onchangescatandsubcat1(request):
    if request.method=='POST':
        catid=request.data['catid']
        print("catid",catid)
        # print("subcatid",subcatid)
        subcat=[]
        if catid :
            for i in catid:
                print(";;;;",i)
                mc=Category.objects.filter(parent=i).values()
                subcat.append(mc)
            return Response(subcat)
        elif catid=='':
            t=Category.objects.filter(parent=None)
            for i in t:
                print(";;;;",i)
                mc=Category.objects.filter(parent=i.id).values()
                subcat.append(mc)
            return Response(subcat)
        
@api_view(['POST'])
def onchangescombinations(request):
    if request.method=='POST':
        
        subcatid=request.data['subcatid']
        pro_opt_id=request.data['pro_opt_id']
        
        pro_var_id=request.data['pro_var_id']
        
        shid=request.data['shid']
        pro=[]
        pr=0
        skpr=0
        
            
        if  subcatid and pro_opt_id=='' and pro_var_id=='':
            pr=Product.objects.filter(shop=shid,subcategory_id=subcatid)
        
        elif subcatid and pro_opt_id and pro_var_id:
            
            skpr=Product.objects.filter(shop=shid,subcategory_id=subcatid)
        
        ps=0
        s=0
        o=0
        f=0
        
        if pr:
            for i in pr:
                
                    sku=Sku.objects.filter(Q(product_id=i.id))
                    print(sku.values())
                    if sku :
                        for j in sku:
                            f=j.id
                            ps=j.price
                            s=j.sales_rate
                            o=j.offer
                            
                        
                        if f!=0:    
                            data={
                                "id":i.id,
                                "skid":f,  
                                "productname":i.product_name,
                                "maincategory":i.maincategory.category_name,
                                "subcategory":i.subcategory_id,
                                "product_image":i.product_image1.url,
                                "brand":i.brand_name,
                                "price":ps,
                                "salesrate":s,
                                "offer":o,
                                "Featured":i.featured
                            }
                            pro.append(data)
            
        elif skpr:
            for i in skpr:
                print("iiiiiiiiiii",i)
                # for proptid in pro_opt_id:
                #     print("proptid",proptid)
                # for provarid in pro_var_id:
                #     print("provarid",provarid)
                sku=Sku.objects.filter(Q(product_id=i.id)&Q(products_options__in=pro_opt_id)&Q(products_variation__in=pro_var_id))

                print("SKPR",sku.values())
                        # if sku :
                        #     for j in sku:
                        #         f=j.id
                        #         ps=j.price
                        #         s=j.sales_rate
                        #         o=j.offer
                        #         # opt=j.products_options
                        #         # var=j.products_variation
                            
                        #     if f!=0:    
                        #         data={
                        #             "id":i.id,
                        #             "skid":f,  
                        #             "productname":i.product_name,
                        #             # "product_options":opt,
                        #             # "product_options_variations":var,
                        #             "maincategory":i.maincategory.category_name,
                        #             "subcategory":i.subcategory_id,
                        #             "product_image":i.product_image1.url,
                        #             "brand":i.brand_name,
                        #             "price":ps,
                        #             "salesrate":s,
                        #             "offer":o,
                        #             "Featured":i.featured
                        #         }
                        #         print("data",data)
                                # pro.append(data)
                            

            return Response({sku.values()})




        
    
        
       
        
        
@api_view(['POST'])
def onchangesfilter(request):
    if request.method=='POST':
        catid=request.data['catid']
        shid=request.data['shid']
        subcatid=request.data['subcatid']
        # comid=request.data['combid']
        pro=[]
        if catid and subcatid =='' :
            pros=Product.objects.filter(maincategory=catid,shop=shid)
            ps=0
            s=0
            o=0
            for i in pros:
                sku=Sku.objects.filter(product_id=i.id)
                for j in sku:
                    ps=j.price
                    s=j.sales_rate
                    o=j.offer
                    skid=j.id
                data={
                    "id":i.id,
                    "skid":skid,
                    "productname":i.product_name,
                    "maincategory":i.maincategory.category_name,
                    "subcategory":i.subcategory_id,
                    "product_image":i.product_image1.url,
                    "brand":i.brand_name,
                    "price":ps,
                    "salesrate":s,
                    "offer":o,
                    "Featured":i.featured
                }
                pro.append(data)
            return Response(pro)
        elif catid and subcatid:
            
            pros=Product.objects.filter(maincategory=catid,shop=shid)
            for sub in subcatid:
                
                pr=pros.filter(subcategory_id=sub)
                ps=0
                s=0
                o=0
                for i in pr:
                    sku=Sku.objects.filter(product_id=i.id)
                    for j in sku:
                        ps=j.price
                        s=j.sales_rate
                        o=j.offer
                        skid=j.id
                        
                    data={
                        "id":i.id,
                        "skid":skid,
                        "productname":i.product_name,
                        "maincategory":i.maincategory.category_name,
                        "subcategory":i.subcategory_id,
                        "product_image":i.product_image1.url,
                        "brand":i.brand_name,
                        "price":ps,
                        "salesrate":s,
                        "offer":o,
                        "Featured":i.featured
                    }
                    pro.append(data)
        return Response(pro)

        
@api_view(['POST'])
def addwishlist(request):
    if request.method=='POST':
        userid=request.data['userid']
        product_id=request.data['product_id']
        sku_id=request.data['sku_id']
        if not Wishlist.objects.filter(userid_id=userid,product_id_id=product_id,sku_id_id=sku_id).exists():

            Wishlist.objects.create(userid_id=userid,product_id_id=product_id,sku_id_id=sku_id)
        else:
            return Response({"Not Exist"})
        
    return Response({"success"})   

@api_view(['GET'])
def listwishlist(request,uid):
    wishlists=Wishlist.objects.filter(userid=uid)
    f=[]
    for i in wishlists:
        
        data={
            "id":i.id,
            "userid":i.userid_id,
            "username":i.userid.username,
            "productid":i.product_id_id,
            "productname":i.product_id.product_name,
            "product_image":i.product_id.product_image1.url,
            "price":i.sku_id.price,
            "stock":i.sku_id.stock,
            "sales_rate":i.sku_id.sales_rate,
            "sku_id":i.sku_id.id
        }
        f.append(data)   
       
    return Response(f)   
@api_view(['GET'])
def deletewishlist(request,wlid):
    if Wishlist.objects.filter(id=wlid).exists():
        k=Wishlist.objects.filter(id=wlid)
        k.delete()
    else:
        return Response({"Wishlist Not exits!!"})
    
       
    return Response({"success"})  

@api_view(['GET'])
def getpricefromsizechange(request,skid):
    j=Sku.objects.filter(id=skid)
    if j:
        for k in j: 
            
            data={
                    "id": k.id,
                    "sku_name": k.sku_name,
                    "product_id_id": k.product_id_id,
                    "price": k.price,
                    "sales_rate": k.sales_rate,
                    "offer": k.offer,
                    "stock": k.stock,
                    "stock_status": k.stock_status,
                    "products_variation_combinations_id_id": k.products_variation_combinations_id_id,
                    "products_variation_combinations_id_secondary_id": k.products_variation_combinations_id_secondary_id

            }
            return Response(data) 
    else:
        
                
        return Response({"status":0})


     
@api_view(['GET'])
def getcombinationfromsubcat(request,subid):
    j=Products_options.objects.filter(category_id_id=subid) 
    
    
    f=[]
    g=[]
    if j:
        for k in j: 
           
            l=k.product_variation_id.variation_name
            m=k.product_variation_id_id
            # print("***********",l)
            f.append(l)
            g.append(m)
        # print(f)
        d=list(set(f))
        print("------",d)
        e=list(set(g))
        print("++++++",e)
        # print("????????",Products_variations_options.objects.filter(product_variation_id_id=e[0]).values())
        # data={
        #     "id":e
        # }
        # print("//////",data)
        choices=[]
        Combinations=[]
        for i,j in enumerate(e):
            print("======",i,j)
            varopt=Products_variation_combinations.objects.filter(product_option_id__product_variation_id=j,category_id_id=subid)
            # getopt=varopt.filter()
            # print("------",varopt.values())
            for pvo in varopt:
                # print("*****",pvo)
                varopt_name=pvo.product_variations_options_id.variationOptionName
                # print("*****",varopt_name) 
                # varopt_id=pvo.id
                # pvid=pvo.product_variation_id_id
                # pvname=pvo.product_variation_id.variation_name
                dat={
                    "id":pvo.product_variations_options_id.id,
                    "name":varopt_name,
                    
                    
                }
                print("]]]]]",dat)
                choices.append(dat)
            
            
            
            vname=Products_variations.objects.filter(id=j)   
            print("vnameggg",vname)
            data1={
                "id":vname[0].id,
                "name":vname[0].variation_name,
                "subcategory_id":subid,
                "choices":choices
            }
             
            Combinations.append(data1) 
            choices=[] 

            print("#####",Combinations)  
        return Response(Combinations) 
    else:
          
                
        return Response({"status":0})

