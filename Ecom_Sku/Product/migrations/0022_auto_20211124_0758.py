# Generated by Django 3.2.9 on 2021-11-24 07:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0021_auto_20211124_0505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='product_image1',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image2',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image3',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
