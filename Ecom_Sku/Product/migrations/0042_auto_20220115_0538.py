# Generated by Django 3.2.9 on 2022-01-15 05:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0041_wishlist_sku_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sku',
            name='products_variation_combinations_id',
        ),
        migrations.RemoveField(
            model_name='sku',
            name='products_variation_combinations_id_secondary',
        ),
        migrations.AddField(
            model_name='sku',
            name='products_otions',
            field=models.ManyToManyField(to='Product.Products_variations'),
        ),
        migrations.AddField(
            model_name='sku',
            name='products_variation',
            field=models.ManyToManyField(to='Product.Products_variations_options'),
        ),
    ]
