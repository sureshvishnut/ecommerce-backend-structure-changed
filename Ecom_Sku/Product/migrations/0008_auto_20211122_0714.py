# Generated by Django 3.2.9 on 2021-11-22 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0007_rename_user_product_shop'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='banner1',
            field=models.ImageField(blank=True, null=True, upload_to='media/products'),
        ),
        migrations.AlterField(
            model_name='product',
            name='banner2',
            field=models.ImageField(blank=True, null=True, upload_to='media/products'),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image1',
            field=models.ImageField(blank=True, null=True, upload_to='media/products'),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image2',
            field=models.ImageField(blank=True, null=True, upload_to='media/products'),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image3',
            field=models.ImageField(blank=True, null=True, upload_to='media/products'),
        ),
    ]
