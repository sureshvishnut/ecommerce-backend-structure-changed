from django.db.models import fields
from rest_framework import serializers

from Customer.serializers import userserializer
from .models import *
from Category.models import *
from Customer.models import *
############## product variations ########

class newserializer2(serializers.ModelSerializer):
    class Meta:
        model=Products_variations
        fields='__all__'
class newserializer1(serializers.ModelSerializer):
    product_variation_id=newserializer2()
    class Meta:
        model=Products_variations_options
        fields=['product_variation_id','variationOptionName']
class newserializer(serializers.ModelSerializer):
    product_variations_options_id=newserializer1()
    class Meta:
        model=Products_variation_combinations
        fields=[
            'id',
            
            'category_id',
            'product_variations_options_id',
        ]
########### sku ############
class Skuserializer(serializers.ModelSerializer):
    products_variation_combinations_id=serializers.SerializerMethodField('get_products_variation_combinations')
    products_variation_combinations_id_secondary = serializers.SerializerMethodField('get_products_variation_combinations_secondary')
    class Meta:
        model = Sku
        fields = ['id','sku_name','price','sales_rate','product_id','stock','offer','products_variation_combinations_id','products_variation_combinations_id_secondary']
        depth=1
    def get_products_variation_combinations(self,obj):
        if obj.products_variation_combinations_id:
            k=Products_variation_combinations.objects.filter(id=obj.products_variation_combinations_id.id)
            serializer = newserializer(k,many=True)
            return serializer.data
    def get_products_variation_combinations_secondary(self,obj):
        if obj.products_variation_combinations_id_secondary:
            k=Products_variation_combinations.objects.filter(id=obj.products_variation_combinations_id_secondary.id)
            serializer = newserializer(k,many=True)
            return serializer.data
            
########### product ############
class zoneserializer(serializers.ModelSerializer):
    def to_representation(self,instance):
            my_fields={'id',
            'zone_name',
            'latitude',
            'longitude',
            'delivery_within_days',
            'delivery_charge',
            'distance'}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model=Zone
        fields=[
            'id',
            'zone_name',
            'latitude',
            'longitude',
            'delivery_within_days',
            'delivery_charge',
            'distance'
        ]
class dresserializer(serializers.ModelSerializer):
    class Meta:
        model=Dresschart
        fields='__all__'

class footserializer(serializers.ModelSerializer):
    class Meta:
        model=Footwearchart
        fields='__all__'
class unitserializer(serializers.ModelSerializer):
    class Meta:
            model=Unit
            fields='__all__'
class pinserializer(serializers.ModelSerializer):
    class Meta:
        model=Pincode
        fields='__all__'
class userserializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=['email']
class productserializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    pincode=pinserializer(many=True, read_only=True)
    zone=zoneserializer(many=True, read_only=True)
    unit=unitserializer()
    sku=serializers.SerializerMethodField('get_sku')
    product_id=serializers.IntegerField(source='id')
    footware_size=serializers.BooleanField(source='footware_sizechart')
    dress_size=serializers.BooleanField(source='dress_sizechart')
    shop=userserializer()
    def to_representation(self,instance):
            my_fields={'id',
            'product_id',
            'product_name',
            'product_image1',
            'product_image2',
            'product_image3',
            'product_description',
            'highlights',
            'return_policy',
            'number_of_days_for_refund',
            'Net_weight',
            'minimum_product_percentage',
            'maincategory',
            'subcategory',
            'delivery_days',
            'banner1',
            'banner2',
            'delivery_charge',
            'brand_name',
            'coupon_code',
            'unit',
            'pincode',
            'zone',
            'footware_size',
            'dress_size',
            'featured',
            'sku',
            
            'shop'}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model=Product
        fields=[
            'id',
            'product_id',
            'product_name',
            'product_image1',
            'product_image2',
            'product_image3',
            'product_description',
            'highlights',
            'return_policy',
            'number_of_days_for_refund',
            'Net_weight',
            'minimum_product_percentage',
            'maincategory',
            'subcategory',
            'delivery_days',
            'banner1',
            'banner2',
            'delivery_charge',
            'brand_name',
            'coupon_code',
            'unit',
            'pincode',
            'zone',
            'footware_size',
            'dress_size',
            'featured',
            'sku',
            'shop',
            
        ]
        depth=1
    def get_sku(self, obj):
        if Sku.objects.filter(product_id=obj.id).exists():
            k=Sku.objects.filter(product_id=obj.id)
            serializer = Skuserializer(k,many=True)
            return serializer.data
        else:
            serializer = [
                {
                "id": "",
                "sku_name": "",
                "price": "",
                "sales_rate": "",
                "product_id": "",
                "stock":"",
                "offer": "",
                "products_variation_combinations_id": [
                    {
                        "id": "",
                        "product_id": "",
                        "category_id": "",
                        "product_variations_options_id": {
                            "product_variation_id": {
                                "id": "",
                                "variation_name": ""
                            },
                            "variationOptionName": ""
                        }
                    }
                ]
            }
            ]
            
            return serializer
class productvariationsoptionserializer(serializers.ModelSerializer):
    class Meta:
            model=Products_variations_options
            fields='__all__'
            depth=1

class skuserializer(serializers.ModelSerializer):
   
    class Meta:
        model = Sku
        fields = ['id','sku_name','price','sales_rate','product_id','stock','offer',]

class skuserializer1(serializers.ModelSerializer):
   
    class Meta:
        model = Sku
        fields=['id','sku_name','products_variation_combinations_id','products_variation_combinations_id_secondary']
        depth=3 
class skuserializercc(serializers.ModelSerializer):
   
    class Meta:
        model = Sku
        fields=['id','sku_name','products_options','products_variation']
        depth=1
  
class pvcserializer(serializers.ModelSerializer):
   
    class Meta:
        model = Products_variation_combinations
        fields="__all__"
        depth=2

class wishlistserializer(serializers.ModelSerializer):
   
    class Meta:
        model = Wishlist
        fields="__all__"
        