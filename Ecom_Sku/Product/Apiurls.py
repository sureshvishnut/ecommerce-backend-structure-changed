from django.contrib import admin
from django.urls import path,include
from . import ApiView
from Product.ApiView import *
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('sizechart/<str:sizechart>',ApiView.sizechart,name='size'), ############# same using for footwear
    path('listProducts/<int:shid>',ApiView.listProducts,name='listProducts'),
    path('listProductscus/<int:shid>',ApiView.listProductscust,name='listProductscus'), 
    path('RetrieveProducts/<int:productid>',ApiView.RetrieveProducts,name='RetrieveProducts'),
    path('RetriveSku/<int:skuid>',ApiView.RetriveSku,name='RetriveSku'),
    path('RetrieveProductsCust/<int:productid>',ApiView.RetrieveProductsCust,name='RetrieveProductsCust'),
    path('RetrieveProductsbyname/',ApiView.RetrieveProductsbyname,name='RetrieveProductsbyname'),
    path('RetrieveProductsbynameandcat/',ApiView.RetrieveProductsbynameAndCat,name='RetrieveProductsbynameandcat'),
    path('onchange/',ApiView.onchange,name="onchange"),
    path('onchangescatandsubcat/',ApiView.onchangescatandsubcat,name="onchangescatandsubcat"),
    path('onchangescatandsubcat1/',ApiView.onchangescatandsubcat1,name="onchangescatandsubcat1"),
    path('getfullsubcat/',ApiView.getfullsubcat,name="getfullsubcat"),
    path('onchangescombinations/',ApiView.onchangescombinations,name="onchangescombinations"),
    path('onchangesfilter/',ApiView.onchangesfilter,name="onchangesfilter"),
    path('RetrieveFeaturedProducts/<int:shid>',ApiView.RetrieveFeaturedProducts,name='RetrieveFeaturedProducts'),
    path('retrieveproduct_bycategory/<int:catid>/<int:shid>/',ApiView.products_by_category,name='products_by_category'),
    path('retrieveproduct_bysubcategory/<int:scatid>/<int:shid>/',ApiView.products_by_subcategory,name='products_by_subcategory'),
    path('productdessort/<int:shid>',ApiView.productdessort,name='productdessort'),
    path('productascsort/<int:shid>',ApiView.productascsort,name='productascsort'),  
    path('productbypricerange',ApiView.productbypricerange,name='productbypricerange'),  
    path('Size/',ApiView.Size,name="Size"),
    path('Color/',ApiView.Color,name="Color"),
    path('listskuofproduct/<int:pid>',ApiView.listskuofproduct,name="listskuofproduct"),
    path('addwishlist/',ApiView.addwishlist,name="addwishlist"),
    path('getwishlist/<int:uid>',ApiView.listwishlist,name="listwishlist"),
    path('deletewishlist/<int:wlid>',ApiView.deletewishlist,name="deletewishlist"),
    path('getpricefromsizechange/<int:skid>',ApiView.getpricefromsizechange,name="getpricefromsizechange"),
    path('getcombinationfromsubcat/<int:subid>',ApiView.getcombinationfromsubcat,name="getcombinationfromsubcat"),
  
]