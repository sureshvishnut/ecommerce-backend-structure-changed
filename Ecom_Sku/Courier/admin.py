from django.contrib import admin

from .models import *


class courierAdmin(admin.ModelAdmin):
    list_display = ['name','shop']
  
    def active(self, obj):
        return obj.is_active == 1
  
    active.boolean = True

admin.site.register(Courier,courierAdmin)