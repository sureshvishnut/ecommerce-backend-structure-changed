from django.shortcuts import render,redirect

from .models import Courier
from Customer.models import*
def addCourier(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')
    else:
        shopid=User.objects.get(email=key)
        print("+++++++++",shopid)
        if request.method == 'POST':
            name=request.POST.get('name')
            address=request.POST.get('address')
            phone=request.POST.get('phone')
            city=request.POST.get('city')
            postcode=request.POST.get('postcode')
            website_url=request.POST.get('website_url')
            Courier.objects.create(name=name,address=address,phone=phone,city=city,postcode=postcode,website_url=website_url,shop=shopid)
            return redirect('listCourier')
        return render(request,"addcourier.html")

def update_courier(request,courierid):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')
    else:
        shopid=User.objects.get(email=key)
        print("+++++++++",shopid)
    
        courier=Courier.objects.filter(id=courierid).values()
        print("xxxxxxxxxxxxxxxxxxx",courier)
        if request.method == 'POST':
            name=request.POST.get('name')
            address=request.POST.get('address')
            phone=request.POST.get('phone')
            city=request.POST.get('city')
            postcode=request.POST.get('postcode')
            website_url=request.POST.get('website_url')
            courier.update(name=name,address=address,phone=phone,city=city,postcode=postcode,website_url=website_url)
            return redirect('listCourier')
        return render(request,"updatecourier.html",{'courier':courier[0]})
    

def listCourier(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')
    else:
        shopid=User.objects.get(email=key)
        print("+++++++++",shopid)
        courier=Courier.objects.filter(shop=shopid)
        if 'delete' in request.GET and request.method =='POST':
            courierid = request.POST.get('courierid')
            get_courier=Courier.objects.filter(id=courierid)
            get_courier.delete()
            return redirect('listCourier')  
        return render(request,'listcourier.html',{'courier':courier})
