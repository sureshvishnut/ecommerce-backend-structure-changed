from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('',views.loginView,name='loginView'),
    path('dashboard',    views.dashboard,    name = 'dashboard'),
    
    path('showdetails/<int:id>',views.ShowDetails,name="showdetails"),
    path('pdf/<int:id>',views.GeneratePDF,name='pdf'),
    path('accept',views.accept,name="accept"),
    path('PushNotification', views.PushNotification,name='PushNotification'),
    path('SMSNotification', views.SMSNotification,name='SMSNotification'),
    path('EmailNotification', views.EmailNotification,name='EmailNotification'),

]
