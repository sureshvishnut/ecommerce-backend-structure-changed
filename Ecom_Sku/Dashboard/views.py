from django.contrib.auth.models import Group
from django.http import response
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from rest_framework.decorators import action
import requests
from django.template.loader import get_template
from xhtml2pdf import pisa
from io import BytesIO
from django.core.files.storage import FileSystemStorage
from Order.models import order,orderdetails
from Customer.models import User
from Product.models import *
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.contrib.auth import get_user_model
User = get_user_model()
from Ecom_Sku.utils import render_to_pdf
# Create your views here.

def loginView(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password= request.POST.get('password')
        user = authenticate(email=email,password=password)
        if user is not None:
            check_user = User.objects.filter(email=email).values()
            data=check_user[0]['email']
            id=check_user[0]['id']
            print("000000000",id)
            print("1111111",data)
            groupsmain=Group.objects.filter(name="shops")
            print("222222",groupsmain[0])
            groupselect=groupsmain[0]
            print("3333333",type(groupselect))
            groupss=user.groups.filter(user=id)
            print("4444444",groupss)
            g1=groupss[0]
            print("555555",type(g1))
            
            if g1 == groupselect:
                
                request.session['user'] = data
                print("+++++++++",request.session['user'])
                return redirect('dashboard')
            else:
                extra = {'popup':True,'heading':'Login Failed','msg':'Access Denied'}
                return render(request,'login.html',{'extra':extra})

        else:
            extra = {'popup':True,'heading':'Login Failed','msg':'Invalid Email  OR Password'}
            return render(request,'login.html',{'extra':extra})
    return render(request,'login.html')

def dashboard(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid)  
        total_order_details=order.objects.filter(orderdetails__product_id__shop=shopid).count()
        print("----------------",total_order_details)
        completed=order.objects.filter(status="Completed",orderdetails__product_id__shop=shopid).count()
        accepted=order.objects.filter(status="Accepted",orderdetails__product_id__shop=shopid).count()
        out_of_delivery=order.objects.filter(status="Out of Delivery",orderdetails__product_id__shop=shopid).count()
        order_cancel=order.objects.filter(status="Order Cancel",orderdetails__product_id__shop=shopid).count()
        shopid=User.objects.get(email=key)
        print("+++++++++",shopid)
        
        completed_list=order.objects.filter(status="Completed",orderdetails__product_id__shop=shopid).order_by('-id')
        out_of_delivery_list=order.objects.filter(status="Out of Delivery",orderdetails__product_id__shop=shopid).order_by('-id')
        accepted_list=order.objects.filter(status="Accepted",orderdetails__product_id__shop=shopid).order_by('-id')
        
        order_cancel_list=order.objects.filter(status="Order Cancel",orderdetails__product_id__shop=shopid).order_by('-id')
        orders=order.objects.filter(orderdetails__product_id__shop=shopid).order_by('-id')
        ordered=orders.filter(status='Ordered',orderdetails__product_id__shop=shopid).count()
        user = request.user.username
        response = order.objects.filter(orderdetails__product_id__shop=shopid).order_by('-id')
        page=request.GET.get('page')
        paginator=Paginator(response,per_page=50)
        shopid=User.objects.get(email=key)
        sh=User.objects.filter(email=key).values()
        shid=sh[0]['id']
        print("shid",shid)
        

        res=User.objects.filter(shopper_id=shid).values()
        print("res",res)

        
        
        customer_list=res
        print("customer_list",res)
        customer=res.count()
        print("customer_list",customer)  
        try:
            response=paginator.page(page)
        except PageNotAnInteger:
            response=paginator.page(1)
        except EmptyPage:
            response=paginator.page(paginator.num_pages)
        return render(request,'dashboard.html',
                                {'total_order_details':total_order_details,
                                'completed':completed,
                                'accepted':accepted,
                                'out_of_delivery':out_of_delivery,
                                'order_cancel':order_cancel,
                                'customers':customer,
                                'customer_list':customer_list,
                                'out_of_delivery_list':out_of_delivery_list,
                                'accepted_list':accepted_list,
                                'order_cancel_list':order_cancel_list,
                                'completed_list':completed_list,
                                'ordered':ordered,
                                'username':user,
                                'response':response,
                                'paginator':paginator})





    
# def loginView(request):
#     if request.method == 'POST':
#         phone=request.POST.get('phone')
#         password=request.POST.get('password')
#         user = authenticate(username=phone, password=password)
#         if  user is not None:
#             login(request,user)
#             k=User.objects.get(phone=phone)
#             print("//////",k)
#             username=k.username
            
#             return redirect(dashboard)
#         else:
#             extra = {'popup':True,'heading':'Login Failed','msg':'Invalid Phone Number OR Password'}
#             return render(request,'login.html',{'extra':extra})
#     return render(request,'login.html')


# #************************order acceptence and cancel ********************#
def accept(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid) 

        if 'action' in request.GET:
            order_id = request.GET.get('id')
            orders = order.objects.get(id = order_id)

            if orders.status == 'Ordered':
                if request.GET.get('action') =='1':
                    orders.status = 'Accepted'
                    orders.save()
                    return redirect("dashboard")

                elif request.GET.get('action') == '0':
                    orders.status = 'Order Cancel'
                    orders.save()
                    return redirect("dashboard")
            else:
                return redirect("dashboard")


            
            return redirect("dashboard")
        
# #********************************************show details**********************************************************#
def ShowDetails(request,id):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        shops=User.objects.get(email=key)
        print("+++++++++",shops) 
    
        orders = order.objects.get(id=id)
        print("xxxxxxxxxxxxxxxx",orders)
        orderd = orders.orderdetails_set.all().values()
        print("yyyyyyyyyyyyyyyyyyy",orderd)
        
        
        pro={}
        sku={}
        pvoop={}
        prefernce={}
        # if(orderd!=""): 
        if((orderd[0]['product_id_id'])!=None):
            proid=orderd[0]['product_id_id']
            pro=Product.objects.filter(id=proid).values()
            
            if((orderd[0]['sku_id'])!=None):
                skuid=orderd[0]['sku_id']
                sku=Sku.objects.filter(id=skuid).values()
                
                if((sku[0]['products_variation_combinations_id_id'])!=None): 
                    per_id=sku[0]['products_variation_combinations_id_id']
                    per=Products_variation_combinations.objects.filter(id=per_id).values()
                    
                    if((per[0]['product_variations_options_id_id']!=None)):
                        pvo=per[0]['product_variations_options_id_id']
                        pvoop=Products_variations_options.objects.filter(id=pvo).values()
                        
                        if((pvoop[0]['product_variation_id_id']!=None)): 
                            pvo1=pvoop[0]['product_variation_id_id']
                            prefernce=Products_variations.objects.filter(id=pvo1).values()
                            
                            return render(request,'invoice.html',{'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce})  
        
        return render(request,'invoice.html',{'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce})
#********************************************generate pdf *************************************************************#
def GeneratePDF(request,id):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid) 
        orders = order.objects.get(id=id)
        
        orderd = orders.orderdetails_set.all().values()
        
        
        
        pro={}
        sku={}
        pvoop={}
        prefernce={}
        if((orderd[0]['product_id_id'])!=None):
            proid=orderd[0]['product_id_id']
            pro=Product.objects.filter(id=proid).values()
            
            if((orderd[0]['sku_id'])!=None):
                skuid=orderd[0]['sku_id']
                sku=Sku.objects.filter(id=skuid).values()
                
                if((sku[0]['products_variation_combinations_id_id'])!=None): 
                    per_id=sku[0]['products_variation_combinations_id_id']
                    per=Products_variation_combinations.objects.filter(id=per_id).values()
                    
                    if((per[0]['product_variations_options_id_id']!=None)):
                        pvo=per[0]['product_variations_options_id_id']
                        pvoop=Products_variations_options.objects.filter(id=pvo).values()
                        
                        if((pvoop[0]['product_variation_id_id']!=None)): 
                            pvo1=pvoop[0]['product_variation_id_id']
                            prefernce=Products_variations.objects.filter(id=pvo1).values()
                            
                            template = get_template('invoice.html')
                            context = {'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce}
                            pdf = render_to_pdf('invoice.html',context)
                            if pdf:
                                response = HttpResponse(pdf,content_type='application/pdf')
                                filename= "Invoice_%s.pdf" %("12341231")
                                content = "inline; filename='%s'" %(filename)
                                if content :
                                    response['Content-Disposition'] = 'attachment; filename="report.pdf"'
                                    return response    
                                return HttpResponse("Not found")
        
        
        template = get_template('invoice.html')
        context = {'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce}
        
        pdf = render_to_pdf('invoice.html',context)
        
        if pdf:
            response = HttpResponse(pdf,content_type='application/pdf')
            
            filename= "Invoice_%s.pdf" %("12341231")
            
            content = "inline; filename='%s'" %(filename)
            
            if content :
                response['Content-Disposition'] = 'attachment; filename="report.pdf"'
                return response    
            return HttpResponse("Not found")

########## Notification ###
from django.shortcuts import render
from .models import *
# Create your views here.
def PushNotification(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid) 
        if request.method =='POST':
            From =request.POST.get('From')
            Host =request.POST.get('Host')
            Port =request.POST.get('Port')
            Username =request.POST.get('Username')
            Password =request.POST.get('Password')
            Subject =request.POST.get('Subject')
            Body =request.POST.get('Body')
            k=Push_Notifications.objects.create(From=From,Host=Host,Port=Port,Username=Username,Password=Password,Subject=Subject,Body=Body)
        return render(request, 'push.html')
def SMSNotification(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid) 
        if request.method =='POST':
            From =request.POST.get('From')
            Host =request.POST.get('Host')
            Port =request.POST.get('Port')
            Username =request.POST.get('Username')
            Password =request.POST.get('Password')
            Subject =request.POST.get('Subject')
            Body =request.POST.get('Body')
            k=SMS_Notifications.objects.create(From=From,Host=Host,Port=Port,Username=Username,Password=Password,Subject=Subject,Body=Body)
        return render(request, 'sms.html')
def EmailNotification(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid) 
        if request.method =='POST':
            From =request.POST.get('From')
            Host =request.POST.get('Host')
            Port =request.POST.get('Port')
            Username =request.POST.get('Username')
            Password =request.POST.get('Password')
            Subject =request.POST.get('Subject')
            Body =request.POST.get('Body')
            k=Email_Notifications.objects.create(From=From,Host=Host,Port=Port,Username=Username,Password=Password,Subject=Subject,Body=Body)
        return render(request, 'email.html')