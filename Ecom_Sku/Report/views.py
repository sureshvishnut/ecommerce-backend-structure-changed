# from django.http.response import HttpResponse
# from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import response
import pandas as pd
import os
import requests
from pathlib import Path
import xlwt
import xlsxwriter
from django.shortcuts import render, redirect
from Product.models import Product, Sku
from Customer.models import User
from Order.models import order, orderdetails
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
# BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# #*********************************list products ********************************#


def reportlistproduct(request):
    key = request.session.get('user')
    print("@@@@@@", key)
    if not key:
        return render(request, 'login.html')
    else:
        shopid = User.objects.get(email=key)
        print("+++++++++", shopid)

        product = Product.objects.filter(shop=shopid).order_by('-id')
        return render(request, 'reportproducts.html', {'products': product})
# #********************************repot search products***************************#


def reportsearchproduct(request):
    key = request.session.get('user')
    print("@@@@@@", key)
    if not key:
        return render(request, 'login.html')
    else:
        shopid = User.objects.get(email=key)
        print("+++++++++", shopid)
        path = os.path.join(BASE_DIR, "media/reports/report.xlsx")
        writer = pd.ExcelWriter(path, engine='xlsxwriter')
        df = pd.DataFrame(Product.objects.filter(shop=shopid).values())
        product = Product.objects.filter(shop=shopid)
        if request.method == 'POST':
            product_name = request.POST.get('product_name')
            if product_name:
                df = pd.DataFrame(Product.objects.filter(
                    product_name__startswith=product_name, shop=shopid).values())
                product = Product.objects.filter(
                    product_name__startswith=product_name, shop=shopid)
        df2 = {'price': '', 'sales_rate': '', 'stock': '',
               'product_variation_combinations_id': ''}
        df = df.append(df2, ignore_index=True)
        df.to_excel(writer, sheet_name='Sheet1', index=False)
        worksheet = writer.sheets['Sheet1']
        for idx, col in enumerate(df):
            series = df[col]
            max_len = max((series.astype(str).map(
                len).max(), len(str(series.name))))+1
            worksheet.set_column(idx, idx, max_len)
            writer.save()
        return render(request, "reportproducts.html", {'products': product})
    # return render(request,"reportproducts.html",{'products':product})
# #**********************************************report customer*******************************************3


def reportlistcustomer(request):
    key = request.session.get('user')

    if not key:
        return render(request, 'login.html')
    else:
        shopid = User.objects.get(email=key)
        sh = User.objects.filter(email=key).values()
        shid = sh[0]['id']
        print("shid", shid)

        res = User.objects.filter(shopper_id=shid).values()
        print("res", res)
        return render(request, 'reportcustomer.html', {'response': res})
# #*******************************************************report search customer *****************************************#


def reportsearchcustomer(request):
    key = request.session.get('user')
    print("@@@@@@", key)
    if not key:
        return render(request, 'login.html')
    else:
        shopid = User.objects.get(email=key)
        print("+++++++++", shopid)
        path = os.path.join(BASE_DIR, "media/reports/reports.csv")
        # writer = pd.ExcelWriter(path, engine='xlsxwriter')
        shopid=User.objects.get(email=key)
        sh=User.objects.filter(email=key).values()
        shid=sh[0]['id']
        print("shid",shid)

        res=User.objects.filter(shopper_id=shid).values()
        print("res",res)
        customer_name = {}

        if 'customer_name' in request.GET:
            customer_name = request.GET['customer_name']
            print("xxxxxxxxxxx", customer_name)
            if customer_name != '':

                res = res.filter(
                    username__startswith=customer_name).values()
                print("xxxxxxxxxxx", res)

            elif customer_name == '':
                shopid=User.objects.get(email=key)
                sh=User.objects.filter(email=key).values()
                shid=sh[0]['id']
                print("shid",shid)

                res=User.objects.filter(shopper_id=shid).values()
                print("res",res)

            df = pd.DataFrame(res)
            # df2 = {'username':'','first_name':'','last_name':'','email':'','phone':'','address':'','pincode':'','role':'','city':''}

            # print("zzzzzzzzzzzzzzzzzzzzzzzz",df2)
            # df = df.append(df2, ignore_index = True)

            df.to_csv(path)

            # worksheet=writer.sheets['Sheet1']
            # for idx,col in enumerate(df):
            #     series=df[col]
            #     max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
            #     worksheet.set_column(idx,idx,max_len)
            # writer.save()
        else:
            df = pd.DataFrame(res)
            # df2 = {'username':'','first_name':'','last_name':'','email':'','phone':'','address':'','pincode':'','role':'','city':''}

            # print("yyyyyyyyyyyyyyyyyyyyy",df2)
            # df = df.append(df2, ignore_index = True)

            df.to_csv(path)

            # worksheet=writer.sheets['Sheet1']
            # for idx,col in enumerate(df):
            #     series=df[col]
            #     max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
            #     worksheet.set_column(idx,idx,max_len)
            # writer.save()

        return render(request, "reportcustomer.html", {'response': res})
# #********************************************************report list sales *************************************************#


def reportlistsales(request):
    key = request.session.get('user')
    print("@@@@@@", key)
    if not key:
        return render(request, 'login.html')
    else:
        shopid = User.objects.get(email=key)
        print("+++++++++", shopid)

        response = order.objects.filter(
            orderdetails__product_id__shop=shopid).values()
        print(response)
        return render(request, "reportsales.html", {'orders': response})
# #********************************************************report search sales ************************************************#


def salesreportsearch(request):
    key = request.session.get('user')
    print("@@@@@@", key)
    if not key:
        return render(request, 'login.html')
    else:
        shopid = User.objects.get(email=key)
        print("+++++++++", shopid)
        path = os.path.join(BASE_DIR, "media/reports/report1.xlsx")
        writer = pd.ExcelWriter(path, engine='xlsxwriter')
        df = pd.DataFrame(order.objects.filter(
            orderdetails__product_id__shop=shopid).values())
        orders = order.objects.filter(
            orderdetails__product_id__shop=shopid).values()
        response = order.objects.filter(
            orderdetails__product_id__shop=shopid).values()
        if request.method == 'POST':
            son = request.POST.get("ordernumber")
            s1 = request.POST.get("startingdate")
            s2 = request.POST.get("endingdate")
            status1 = request.POST.get("status")

            if (s1 and s2) != '':
                df = pd.DataFrame(orders.filter(Q(order_date__range=(s1, s2)) & Q(
                    status__contains=status1) & Q(id__contains=son)).values())
                response = orders.filter(Q(order_date__range=(s1, s2)) & Q(
                    status__contains=status1) & Q(id__contains=son)).values()

            else:
                s1 = "2000-01-01"
                s2 = "5000-01-01"
                df = pd.DataFrame(orders.filter(Q(order_date__range=(s1, s2)) & Q(
                    status__contains=status1) & Q(id__contains=son)).values())
                response = orders.filter(Q(order_date__range=(s1, s2)) & Q(
                    status__contains=status1) & Q(id__contains=son)).values()

        # df2 = {'id':'','order_date':'','order_total':'','subtotal':'','customer_id':'','product_id':'','paid':''}

        # df = df.append(df2, ignore_index = True)
        df.to_excel(writer, sheet_name='Sheet1', index=False)
        worksheet = writer.sheets['Sheet1']
        for idx, col in enumerate(df):
            series = df[col]
            max_len = max((series.astype(str).map(
                len).max(), len(str(series.name))))+1
            worksheet.set_column(idx, idx, max_len)
        writer.save()

        return render(request, "reportsales.html", {'orders': response})
