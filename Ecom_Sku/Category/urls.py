from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('categorylist', views.categorylist,name = 'categorylist'),
    path('add_Category', views.add_Category,name = 'add_Category'),
    path('update_category',views.update_Category,name="update_category"),
    path('updatecategory/<int:catid>',views.updatecategory,name="updatecategory"),
    # path('searchCategory',views.searchCategory,name="searchCategory"),
    path('combinations/<int:catid>',views.combinations,name='combinations'),
    # ## Banners
    path('addWebbanner',views.addWebbanner,name="addWebbanner"),
    path('listWebbanners',views.listWebbanners,name="listWebbanners"),
    path('updateWebbanner/<int:bannerid>',views.updateWebbanner,name="updateWebbanner"),
    path('update_banner',views.update_banner,name="update_banner"),
    path('addMobilebanner',views.addMobilebanner,name="addMobilebanner"),
    path('listMobilebanners',views.listMobilebanners,name="listMobilebanners"),
    path('updateMobilebanner/<int:bannerid>',views.updateMobilebanner,name="updateMobilebanner"),
    path('update_mobilebanner',views.update_mobilebanner,name="update_mobilebanner"),
    # ################# Brand logo #############
    path('addOrganization',views.AddOrganization,name='addOrganization'),
    path('listOrganization',views.ListOrganization,name="listOrganization"),
    
    path('updateorganization/<int:orgid>',views.UpdateOrganization,name="updateorganization")

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    