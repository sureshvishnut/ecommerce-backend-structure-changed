from django.http.response import HttpResponse
from django.shortcuts import render
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from rest_framework.response import Response 
from rest_framework.decorators import api_view,permission_classes
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from itertools import zip_longest
from . models import *
from .serializers import  *
### List of all category exclude banners
@api_view(['GET'])
def categorylists(request):
    k=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id')
    serializer=subserializer1(k,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def listcategorys(request):
    k=Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id').values()
    print(k[0])
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def maincategory(request,catid):
    k=Category.objects.filter(parent=None,id=catid).order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def subcategory(request,catid):
    k=Category.objects.filter(parent__isnull=False,id=catid).order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def listcategoryscust(request):
    k=Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id')
    l=[]
    r=[]
    for i in k:
        
        
        j=i.category_name
        k=i.id
        print(j)
       
        s=Category.objects.filter(parent=i).values()
       
        serializer=pcatserializer2x(s,many=True)
        data = {
                
                "maincategory":j,
                "maincatid":k,
                "subcategory":s
                
                    }
           
        l.append(data)    
        
    
    
      
    
    return Response(l)
### Subcategory list
@api_view(['GET'])
def subcategorylists(request):
    k=Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
    serializer=subserializer1(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def retrievecategory_byparents(request,catid):
    k=Category.objects.filter(parent__id=catid).exclude(category_name__contains="Banner")
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)

### Web banner list
@api_view(['GET'])
def bannerlists(request):
    k=Category.objects.filter(category_name__startswith='Banner').order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
### Mobile banner list
@api_view(['GET'])
def mobile_bannerlists(request):
    k=Category.objects.filter(category_name__startswith='Mobile_Banner').order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def retrievecat(request,catid):
    k=Category.objects.filter(id=catid)
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def catsubcat(request):
    j=Category.objects.filter(parent=None).exclude(category_name__contains="Banner").values()
    k=Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner").values()
    
    return Response({j,k})