from Product.views import addPincode
from django.contrib import admin
from .models import * 
# Register your models here.
from .models import *
admin.site.register(Category)
class orgAdmin(admin.ModelAdmin):
    list_display = ['company_name','shop']
  
    def active(self, obj):
        return obj.is_active == 1
  
    active.boolean = True
admin.site.register(addOrganization,orgAdmin)