from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('listOrders',views.listOrders,name="listOrders"),
    path('update_order/<int:orderid>',views.update_order,name="update_order"),
    path('updateOrders',views.updateOrders,name="updateOrders"),
    path('ordersearch',views.ordersearch,name="ordersearch"),
]