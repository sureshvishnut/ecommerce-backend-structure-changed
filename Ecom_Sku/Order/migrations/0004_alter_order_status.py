# Generated by Django 3.2.9 on 2021-11-23 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Order', '0003_alter_order_order_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(blank=True, choices=[('Completed', 'Completed'), ('Ordered', 'Ordered'), ('Accepted', 'Accepted'), ('Order Cancel', 'Order Cancel'), ('Customer Cancel', 'Customer Cancel'), ('Delivered', 'Delivered'), ('Added to Cart', 'Added to Cart'), ('Out of Delivery', 'Out of Delivery'), ('Refund Initiated', 'Refund Initiated'), ('Return And exchange', 'Return And exchange')], default='1', max_length=50, null=True),
        ),
    ]
