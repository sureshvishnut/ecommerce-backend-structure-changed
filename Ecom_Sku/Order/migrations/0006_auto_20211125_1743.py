# Generated by Django 3.2.9 on 2021-11-25 17:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Order', '0005_alter_order_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='Is_delivered',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='order',
            name='payment_status',
            field=models.BooleanField(default=False),
        ),
    ]
