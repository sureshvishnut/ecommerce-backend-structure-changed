from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('addCoupon',views.addCoupon,name="addCoupon"),
    path('listCoupon',views.listCoupon,name="listCoupon"),
    path('updateCoupon/<int:couponid>',views.updateCoupon,name="updateCoupon"),
    path('update_Coupon',views.update_Coupon,name="update_Coupon"),
    
]