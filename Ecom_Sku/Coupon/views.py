
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from rest_framework.response import Response 
from . models import Couponcode
from Customer.models import *


def addCoupon(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:

        
        shopid=User.objects.get(email=key)
        
        print("+++++++++",shopid)
        if request.method == 'POST':
            coupon_name = request.POST.get('coupon_name')
            coupon      = request.POST.get('coupon')
            flat_value  =  request.POST.get('flat_value')
            response=Couponcode.objects.create(coupon_name=coupon_name,coupon=coupon,flat_value=flat_value,shop=shopid)
            return redirect('listCoupon')
        return render(request,"coupon.html")

def listCoupon(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:

        
        shop=User.objects.filter(email=key).values()
        shopid=shop[0]['id']
        print("+++++++++",shopid)
        coupon=Couponcode.objects.filter(shop=shopid).order_by('-id')
        if 'delete' in request.GET and request.method =='POST':
            couponid = request.POST.get('couponid')
            print("couponid",couponid)
            coupon=Couponcode.objects.filter(id=couponid)  
            coupon.delete()
            return redirect('listCoupon')  
        return render(request,"listcoupon.html",{'coupon':coupon})

def updateCoupon(request,couponid):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        coupon_=Couponcode.objects.filter(id=couponid)
        return render(request,'coupon_update.html',{'coupon_':coupon_})

def update_Coupon(request):
    key=request.session.get('user')
    print("@@@@@@",key)
    if not key:
        return render(request,'login.html')

    else:
        update_coupon={}
        if request.method == 'POST':
            couponid = request.POST.get('couponid')
            coupon_=Couponcode.objects.filter(id=couponid)
            flat_value  = request.POST.get('flat_value')
            coupon_name = request.POST.get('coupon_name')
            if coupon_name:
                update_coupon["coupon_name"]=coupon_name
            coupon      = request.POST.get('coupon')
            if coupon:
                update_coupon["coupon"] = coupon
            if flat_value:
                update_coupon["flat_value"]=flat_value
            coupon_.update(**update_coupon)
            return redirect('listCoupon')
