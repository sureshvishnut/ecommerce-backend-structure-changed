from django.contrib import admin
from .models import *

class couponAdmin(admin.ModelAdmin):
    list_display = ['coupon_name','shop']
  
    def active(self, obj):
        return obj.is_active == 1
  
    active.boolean = True
admin.site.register(Couponcode,couponAdmin)