from django.db import models
from Customer.models import *
# Create your models here.
class Couponcode(models.Model):
    coupon_name     =   models.CharField(max_length=100,null=True,blank=True)
    coupon          =   models.IntegerField(null=True,blank=True)
    flat_value          =   models.IntegerField(null=True,blank=True)
    shop                    =   models.ForeignKey('Customer.User',on_delete=models.CASCADE,null=True,blank=True)
    def __str__(self):
            return (str(self.coupon_name))