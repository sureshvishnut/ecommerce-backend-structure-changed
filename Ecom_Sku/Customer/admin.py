from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import *
ADDITIONAL_USER_FIELDS = (
    (None, {'fields': ('phone','address','google_client_id',
    'google_sceret_key','facebook_client_id','facebook_sceret_key',
    'facebook_link','whatsapp_link','instagram_link',
    'google_link','payment_link','merchant_key','shopper_id')}),
)

class MyUserAdmin(UserAdmin):
    model = User

    add_fieldsets = UserAdmin.add_fieldsets + ADDITIONAL_USER_FIELDS
    fieldsets = UserAdmin.fieldsets + ADDITIONAL_USER_FIELDS

admin.site.register(User,MyUserAdmin)
