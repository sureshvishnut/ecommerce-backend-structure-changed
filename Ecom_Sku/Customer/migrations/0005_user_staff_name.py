# Generated by Django 3.2.9 on 2021-12-01 10:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Customer', '0004_remove_user_staff_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='staff_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
